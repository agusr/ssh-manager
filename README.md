# simple ssh manager - shman

shman - simple ssh manager for (Linux, Darwin, Windows)

# Features
* Universal bash scripting
* Very portable and easy to use
* Sorting base on alias, ip/hostname or Note
* Identity (private key) option


# Installation
```sh
$ mkdir $HOME/bin 
$ curl -o $HOME/bin/shman https://gitlab.com/agusr/ssh-manager/raw/master/shman
$ curl -o $HOME/shman-compeltion.bash https://gitlab.com/agusr/ssh-manager/raw/master/shman-compeltion.bash

$ chmod 750 $HOME/bin/shman
$ source $HOME/shman-compeltion.bash
$ export PATH=$PATH:$HOME/bin

echo 'export PATH=$PATH:$HOME/bin' >> $HOME/.bash_profile
echo 'source $HOME/shman-compeltion.bash' >> $HOME/.bash_profile
```

# Usage
```sh
iMAC:~ agusr$ shman -h
Usage: shman <command>

Commands: 
   [enter]        # List all servers
   <alias>        # Connect to server
   ping           # ping all server
   export         # export config
   sort <note|ip> # Sort by Note or Ip Address
   del <alias>    # Delete server
   add <alias>:<user>:<host>:[port]:[note]
```

# Examples:
```sh
 $ shman add nms1:root:127.0.0.1:2222:yk-nms.G.c7
 $ shman nms1
 $ shman del nms1
```

#### Basic bash autocompletion
```sh
iMAC:~ agusr$ shman [tab] [tab]
nms1 nms2 nms3 nms4 ping export sort

```

![](sceenshot_shman.png)



Original script by Errol Byrd
Copyright (c) 2010, Errol Byrd 

Mods & enhance by 0_ agusr@as55666