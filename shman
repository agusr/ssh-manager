#!/bin/bash
# set -x # debug
#########################################
# Original script by Errol Byrd
# Copyright (c) 2010, Errol Byrd <errolbyrd@gmail.com>
#########################################
# Mods & Optimized by 0_ agusr@as55666

#================== Globals ==================================================
# Version
VERSION="06.2"

# Configuration
PRIVATE_KEY="$HOME/.ssh/id_rsa"
HOST_FILE="$HOME/.ssh_servers"
DATA_DELIM=":"
DATA_ALIAS=1
DATA_HUSER=2
DATA_HADDR=3
DATA_HPORT=4
PING_DEFAULT_TTL=20
SSH_DEFAULT_PORT=22
Sortopt=""
# Save IFS
OLDIFS=$IFS
#================== Functions ================================================

function exec_ping() {
    case $(uname) in
        Darw*)
            ping -c1 -t1 -m $PING_DEFAULT_TTL $@
            ;;
        MINGW*)
            ping -n 1 -i $PING_DEFAULT_TTL $@
            ;;
        *)
            ping -c1 -t$PING_DEFAULT_TTL $@
            ;;
    esac
}

function test_host() {
    exec_ping $* > /dev/null
    if [ $? != 0 ] ; then
        echo -n "["
        cecho -n -red "KO"
        echo -n "]"
    else
        echo -n "["
        cecho -n -green "UP"
        echo -n "]"
    fi
}

function sorting () {

# if config file doesn't exist
if [ ! -f $HOST_FILE ]; then touch "$HOST_FILE"; fi
        if [ -n "$PRIVATE_KEY" ] && [ -f $PRIVATE_KEY ]; then
            echo "ssh-Key: $PRIVATE_KEY"
        elif [ -f "${HOME}/.ssh/id_rsa" ]; then
            echo "ssh-key: ${HOME}/.ssh/id_rsa"
        else
            echo "ssh-Key: "
        fi
    echo "List servers: "
    separator
while IFS=: read label user ip port desc
    do
    cecho -n -blue $label
    echo -ne '\t   ==>\t'
    cecho -n -red $user
    cecho -n -yellow "@"
    cecho -n -green $ip
    echo -ne ':'
    if [ "$port" == "" ]; then
        port=$SSH_DEFAULT_PORT
    fi
    cecho -magenta $port
    echo -ne "\t"
    cecho -blue $desc
    echo
done < $HOST_FILE |column -t | sort $Sortopt
echo
}

function separator () {
    echo -e "-------------------------------------------------"
}

function list_commands () {
printf 'Usage: shman <command>

Commands: 
   [enter]        # List all servers
   <alias>        # Connect to server
   ping           # ping all server
   export         # export config
   sort <note|ip> # Sort by Note or Ip Address
   del <alias>    # Delete server
   add <alias>:<user>:<host>:[port]:[note]


Examples:
 $ shman add nms1:root:127.0.0.1:2222:yk-nms1.c7
 $ shman nms1
 $ shman del nms1
\n'
}

function probe () {
    als=$1
    grep -w -e "^${als}" $HOST_FILE > /dev/null
    return $?
}

function get_raw () {
    als=$1
    grep -w -e "${als}" $HOST_FILE 2> /dev/null
}

function get_addr () {
    als=$1
    get_raw "^${als}" | awk -F "$DATA_DELIM" '{ print $'$DATA_HADDR' }'
}

function get_port () {
    als=$1
    get_raw "^${als}" | awk -F "$DATA_DELIM" '{ print $'$DATA_HPORT'}'
}

function get_user () {
    als=$1
    get_raw "^${als}" | awk -F "$DATA_DELIM" '{ print $'$DATA_HUSER' }'
}

function server_add() {
    probe "$alias"
    if [ $? -eq 0 ]; then
        as  echo "$0: alias '$alias' is in use"
    else
        echo "$alias$DATA_DELIM$user" >> $HOST_FILE
        echo "new alias '$alias' added"
    fi
}

function cecho() {
    while [ "$1" ]; do
        case "$1" in
            -normal)        color="\033[00m" ;;
            -black)         color="\033[30;01m" ;;
            -red)           color="\033[31;01m" ;;
            -green)         color="\033[32;01m" ;;
            -yellow)        color="\033[33;01m" ;;
            -blue)          color="\033[34;01m" ;;
            -magenta)       color="\033[35;01m" ;;
            -cyan)          color="\033[36;01m" ;;
            -white)         color="\033[37;01m" ;;
            -n)             one_line=1;   shift ; continue ;;
            *)              echo -n "$1"; shift ; continue ;;
        esac
    shift
    echo -en "$color"
    echo -en "$1"
    echo -en "\033[00m"
    shift
done
if [ ! $one_line ]; then
    echo
fi
}

#=============================================================================

cmd=$1
alias=$2
user=$3

# if config file doesn't exist
# without args
if [ $# -eq 0 ]; then
sorting
exit 0
fi

case "$cmd" in
    # Add new alias
    add )
        server_add
        ;;
    # Export config
    export )
        echo "# file: $HOST_FILE"
        cat $HOST_FILE
        ;;
    # Delete ali
    del )
        probe "$alias"
        if [ $? -eq 0 ]; then
            cat $HOST_FILE | sed '/^'$alias$DATA_DELIM'/d' > /tmp/.tmp.$$
            mv /tmp/.tmp.$$ $HOST_FILE
            echo "alias '$alias' removed"
        else
            echo "$0: unknown alias '$alias'"
        fi
        ;;
    -h | --help )
        list_commands
        ;;
    ping )
        separator 
        echo "ICMP check "
        separator
        while IFS=: read label user ip port         
            do
                test_host $ip
                echo -ne "   "
                cecho -n -blue $label
                echo -ne '  ->  '
                cecho -n -white $ip
                echo
        done < $HOST_FILE
        exit 0
        ;;
    # Connect to host or unknown cmd...
    sort )
    # extra args.
        if [ "$2" == "note" ]; then
            Sortopt="-k 4,4"
        elif [ "$2" == "ip" ]; then
            Sortopt="-k 3,3"
        else
            Sortopt=""
            list_commands
            exit 1
        fi
        sorting
        ;;
    * )
        alias=$cmd
        probe "$alias"
        if [ $? -eq 0 ]; then
            if [ "$user" == ""  ]; then
                user=$(get_user "$alias")
            fi
            addr=$(get_addr "$alias")
            port=$(get_port "$alias")
            # Use default port when parameter is missing
            if [ "$port" == "" ]; then
                port=$SSH_DEFAULT_PORT
            fi
            echo "connecting to '$alias' ($addr:$port)"
            if [ -n $PRIVATE_KEY ] && [ -f $PRIVATE_KEY ]; then
                ssh -i $PRIVATE_KEY -l $user $addr -p $port
            else
                ssh -l $user $addr -p $port
            fi
            
        else
            #echo "$0: unknown cmd '$alias'" default help aja..
            list_commands
            exit 1
        fi
        ;;
esac
# Set IFS back to original value
IFS=$OLDIFS