#/usr/bin/env bash
_shman_completions()
{
  if [ "${#COMP_WORDS[@]}" != "2" ]; then
    return
  fi

  # keep the suggestions in a local variable
  local suggestions=($(compgen -W "$(cut -d: -f1 "$HOME/.ssh_servers") ping sort export" -- "${COMP_WORDS[1]}"))
  COMPREPLY=("${suggestions[@]}")
}

complete -F _shman_completions shman
